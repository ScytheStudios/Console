Better Print Allows The User Access To New Functions That Make Printing Text To The Console Easier

NOTE : If You Don't Want The Program To Break, We Suggest That You Don't Change The Code Inside Any Of The Functions

    exit_on_return();
        Prevents The Console From Automatically Closing - Closes Once The User Presses Enter
        
    delay(SECONDS)
        Pauses The Console For A Specific Amount Of Time In Seconds, It Allows Floats E.G. (2.57) Seconds
        
    print(INPUT);
        Prints Text To The Console - Prints Similar To How A User Would Type (Letter By Letter)
        Don't Worry It Prints Each Letter Very Quickly, It Makes Reading The Text More Interactive
        
    println(INPUT);
        Same As print() However It Inserts A New Line After The Text Has Been Printed
        NOTE : backspace() Will Not Work For Text That Has Been Parsed Through This Function
        
    backspace(ITERATIONS);
        Allows The User To Remove A Specific Amount Of Characters From Printed Text In The Console
                                             
    newline();
        Creates A Break In The Console Allowing Text To Be Printed To A New Line